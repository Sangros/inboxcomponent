export interface InboxViewModel {
    from: From;
    subject: string;
    body: string;
    imageprofileurl: string;
    color: string;
    label: string;
    datetime: string;
}
export interface From {
    name: string;
    email: string;
}

