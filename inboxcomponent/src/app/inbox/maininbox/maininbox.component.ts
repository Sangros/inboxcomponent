import { Component, OnInit } from '@angular/core';
import inboxMockJson from '../../../assets/data/inbox.json';
import { InboxViewModel } from '../models/inbox.model.js';

@Component({
  selector: 'app-maininbox',
  templateUrl: './maininbox.component.html',
  styleUrls: ['./maininbox.component.css']
})
export class MaininboxComponent implements OnInit {
  inboxViewModel: InboxViewModel[];
  constructor() { }

  ngOnInit() {
    this.inboxViewModel = inboxMockJson;
  }

}
