import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaininboxComponent } from './maininbox.component';


describe('MaininboxComponent', () => {
  let component: MaininboxComponent;
  let fixture: ComponentFixture<MaininboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaininboxComponent ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaininboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

describe('+initiallizer', () => {
  it('-should create', () => {
    expect(component).toBeTruthy();
  });
  it('-after call innitializer, should be call without error', () => {
    spyOn(component,"ngOnInit").and.callThrough();
    component.ngOnInit();
    expect(component.ngOnInit).toHaveBeenCalled();
  });
  it('-after call ngOnInit, object inboxViewModel should be not null', () => {
    expect(component.inboxViewModel).not.toBeNull();
  });
});

});
