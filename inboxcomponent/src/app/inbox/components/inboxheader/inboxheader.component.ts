import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inboxheader',
  templateUrl: './inboxheader.component.html',
  styleUrls: ['./inboxheader.component.css']
})
export class InboxheaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onclickIconFilter()
  {
    document.getElementById("myDropdown").classList.toggle("show");
  }
}
