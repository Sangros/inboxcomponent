import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxheaderComponent } from './inboxheader.component';

describe('InboxheaderComponent', () => {
  let component: InboxheaderComponent;
  let fixture: ComponentFixture<InboxheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InboxheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
