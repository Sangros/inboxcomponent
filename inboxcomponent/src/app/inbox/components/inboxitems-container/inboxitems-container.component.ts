import { Component, OnInit, Input } from '@angular/core';
import { InboxViewModel } from '../../models/inbox.model';

@Component({
  selector: 'app-inboxitems-container',
  templateUrl: './inboxitems-container.component.html',
  styleUrls: ['./inboxitems-container.component.css']
})
export class InboxitemsContainerComponent implements OnInit {
  isIconCircleEnable: boolean = false;
  isInboxChecked: boolean = false;
  isImageCircleEnable: boolean = true;
  @Input() inboxItemModel: InboxViewModel;
  constructor() { }

  ngOnInit() {
  }

  onMouseOutInbox() {
    if (this.isIconCircleEnable == true && this.isInboxChecked == false) {
      this.isImageCircleEnable = true;
      this.isIconCircleEnable = false;
      this.isInboxChecked = false;
    }
  }

  onMouseOverInbox() {
    if (this.isIconCircleEnable == false && this.isInboxChecked != true) {
      this.isIconCircleEnable = true;
      this.isImageCircleEnable = false;
    }
  }

  onClickCircleInbox() {
    this.isIconCircleEnable = false;
    this.isInboxChecked = true;
    this.isImageCircleEnable = false;
  }

  onClickCircleInboxChecked() {
    this.isInboxChecked = false;
    this.isIconCircleEnable = true;
    this.isImageCircleEnable = false;
  }

}
