import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxitemsContainerComponent } from './inboxitems-container.component';

describe('InboxitemsContainerComponent', () => {
  let component: InboxitemsContainerComponent;
  let fixture: ComponentFixture<InboxitemsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InboxitemsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxitemsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
