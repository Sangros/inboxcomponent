import { Pipe, PipeTransform } from '@angular/core';
import { SlicePipe } from '@angular/common';

@Pipe({
  name: 'longTextDisplay'
})
export class LongTextDisplayPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let result: string = value;
    let limit = args ? args : 40;
    let slice = new SlicePipe();
    if (result) {
      if (result.length >= limit) {
        result = slice.transform(result, 0, limit);
        result += '...';
      }
    }
    return result;
  }
}
