import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaininboxComponent } from './inbox/maininbox/maininbox.component';


const routes: Routes = [
  {path: '',redirectTo: 'inbox',pathMatch: 'full'},
  {path: 'inbox',component: MaininboxComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
