import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaininboxComponent } from './inbox/maininbox/maininbox.component';
import { InboxheaderComponent } from './inbox/components/inboxheader/inboxheader.component';
import { InboxitemsContainerComponent } from './inbox/components/inboxitems-container/inboxitems-container.component';
import { LongTextDisplayPipe } from './shared/pipes/long-text-display.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MaininboxComponent,
    InboxheaderComponent,
    InboxitemsContainerComponent,
    LongTextDisplayPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  exports: [
    LongTextDisplayPipe
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
