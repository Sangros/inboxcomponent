1.Structure of Project
*src
+---*app
|   +--*inbox
|   |  +--*components
|   |  |  +--*inboxheader
|   |  |  +--*inboxitems-container
|   |  +--*maininbox
|   |  +--*models
|   |  +--*services
|   |--app.module.ts
|   |--app-routing.module.ts
|   +--*shares
|   |  +--*constants
|   |  +--*models
|   |  +--*pipes
|   |  +--*services
|--index.html

2.Seperate component follow by inbox-component.png
3.ng serve should be like this Output-Inbox-component.png